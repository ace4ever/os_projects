// CSE430 Operating Systems
// Project 2: Context Switching
// Partners: (1)Akshay Kalghatgi(1208110614)	(2)Kyle Smith

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "threads.h"

ElementPtr RunQ;

void *loop1(void)
{
	int i;
	while(1)
	for(i=1;i<100;i++)
	{
		printf("\t%d",i);
		if(i%8 == 0)
		{
			printf("\n");
			yield(); 
		}
		usleep(500000);
	}
}

void *loop2(void)
{
	int i;
	while(1)
	for(i=201;i<300;i++)
	{
		printf("\t%d",i);
		if(i%4 == 0)
		{
			printf("\n");
			yield(); 
		}
		sleep(1);
	}
}

void *loop3(void)
{
	int i;
	while(1)
	for(i=501;i<600;i++)
	{
		printf("\t%d",i);
		if(i%15 == 0)
		{
			printf("\n");
			yield(); 
		}
		usleep(500000);
	}
}

int main()
{
	InitQueue(&RunQ);

	printf("\nRunning threads:\n");
	start_thread(&loop1);
	start_thread(&loop2);
	start_thread(&loop3);

	run();

	return 0;
}
