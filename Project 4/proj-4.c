// CSE430 Operating Systems
// Project 3: Semaphore Implementation
// Partners: (1)Akshay Kalghatgi(1208110614)	(2)Kyle Smith(1203754352)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sem.h"


ElementPtr RunQ;
int rc = 0;
sem wsem,mutex;

void *writer(void)
{
	while(1)
	{
		// Writer entry
		P(&wsem);

		printf("\tWriter wrote a line\n");
		fflush(stdout);
		sleep(1);

		// Writer exit
		V(&wsem);
	}
}

void *reader(void)
{
	while(1)
	{
		// Reader entry
		P(&mutex);
		rc++;
		if(rc == 1) P(&wsem);
		V(&mutex);

		printf("\tReader read a line\n");
		fflush(stdout);
		sleep(1);

		// Reader exit
		P(&mutex);
		rc--;
		if(rc == 0) V(&wsem);
		V(&mutex);
	}
}

int main()
{
	InitQueue(&RunQ);
	InitSem(&mutex, 1);
	InitSem(&wsem, 1);
	
	printf("\nRunning threads:\n");



	start_thread(&writer);
	start_thread(&reader);
	start_thread(&reader);
	start_thread(&reader);
	start_thread(&writer);
	run();

	return 0;
}
