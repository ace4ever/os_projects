// CSE430 Operating Systems
// Project 3: Semaphore Implementation
// Partners: (1)Akshay Kalghatgi(1208110614)	(2)Kyle Smith(1203754352)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sem.h"

#define count 5

ElementPtr RunQ;
int buff[count], in=0, out=0, x=0;
sem empty,full;

void *producer(void)
{
	while(1)
	{
		P(&empty);
		buff[in] = x++;
		printf("\tProduced: %d\n", buff[in]);
		fflush(stdout);
		sleep(1);
		in = (in + 1) % count;
		V(&full);
	}

}

void *consumer(void)
{
	while(1)
	{
		P(&full);
		printf("\tConsumed: %d\n",buff[out]);
		fflush(stdout);
		out = (out + 1) % count;
		sleep(1);
		V(&empty);
	}
}

int main()
{
	InitQueue(&RunQ);
	InitSem(&empty, count);
	InitSem(&full, 0);
	
	printf("\nRunning threads:\n");



	start_thread(&consumer);
	start_thread(&consumer);
	start_thread(&producer);
	start_thread(&producer);
	run();

	return 0;
}
