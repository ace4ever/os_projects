#ifndef Semaphores_Included					// C include guard
#define Semaphores_Included

#include "threads.h"

extern ElementPtr RunQ;

typedef struct {
	int sem_value;
	ElementPtr tcb_block_q;
}sem;

void InitSem(sem *s, int x)
{
	s->sem_value = x;
	s->tcb_block_q = NULL;
}

void P(sem *s)
{
	s->sem_value -= 1;
		
	if(s->sem_value < 0)
	{
		ElementPtr currentContext = RunQ;
		AddQueue(&(s->tcb_block_q), DelQueue(&RunQ));
       		swapcontext(&(currentContext->context), &(RunQ->context));
	//	run();
	}
}

void V(sem *s)
{
	s->sem_value += 1;


	if(s->sem_value <= 0)
        	AddQueue(&RunQ, DelQueue(&(s->tcb_block_q)));

	yield();

}

#endif
