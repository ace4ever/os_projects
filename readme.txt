Arizona State University
CSE430 - Operating Systems

Team: Kyle Smith, Akshay Kalghatgi

This is a series of 4 projects, each building on top of the previous.
In project 3 we implement the producer-consumer solution for handling numerous producer and consumer threads.
In project 4 we implement the readers-writers solution for handling numerous reader and writer threads.